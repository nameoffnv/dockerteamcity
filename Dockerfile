FROM ubuntu:14.04

ENV TEAM_CITY_VERSION 9.0.5
ENV TEAMCITY_DATA_PATH /opt/teamcity-data

RUN apt-get update
RUN apt-get install -y wget default-jre
RUN mkdir /opt/teamcity
RUN wget -O - http://download-ln.jetbrains.com/teamcity/TeamCity-$TEAM_CITY_VERSION.tar.gz | tar xzf - -C /opt/teamcity

EXPOSE 8111
VOLUME ["/opt/teamcity-data"]

CMD /opt/teamcity/TeamCity/bin/teamcity-server.sh run